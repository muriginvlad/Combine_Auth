//
//  AuthView.swift
//  Combine_Auth
//
//  Created by Владислав on 19.02.2021.
//

import UIKit
import Combine
import SnapKit

protocol AuthView: UIView {
    
    var events: PassthroughSubject<LoginScreenViewEvent, Never> { get }
    
    @discardableResult
    func update(with state: LoginScreenViewInputData) -> Self
    
}

final class AuthViewImpl: UIView, AuthView {
    
    let events = PassthroughSubject<LoginScreenViewEvent, Never>()
    var selfView = self
    
    var label: UILabel = {
        let label = UILabel()
        label.text = "Hi"
        label.textAlignment = .center
        return label
    }()
    
    var counter: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.isHidden = true
        return label
    }()
    
    var phone: UITextField = {
        let phone = UITextField()
        phone.placeholder = "Enter phone"
        phone.translatesAutoresizingMaskIntoConstraints = false
        phone.keyboardType = UIKeyboardType.default
        phone.returnKeyType = UIReturnKeyType.done
        phone.autocorrectionType = UITextAutocorrectionType.no
        phone.font = UIFont.systemFont(ofSize: 13)
        phone.borderStyle = UITextField.BorderStyle.roundedRect
        phone.clearButtonMode = UITextField.ViewMode.whileEditing;
        phone.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        phone.addTarget(self, action: #selector(formDidChange), for: .editingChanged)
        return phone
    }()
    
    var code: UITextField = {
        let code = UITextField()
        code.placeholder = "Enter code"
        code.translatesAutoresizingMaskIntoConstraints = false
        code.keyboardType = UIKeyboardType.default
        code.returnKeyType = UIReturnKeyType.done
        code.autocorrectionType = UITextAutocorrectionType.no
        code.font = UIFont.systemFont(ofSize: 13)
        code.borderStyle = UITextField.BorderStyle.roundedRect
        code.clearButtonMode = UITextField.ViewMode.whileEditing;
        code.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        code.addTarget(self, action: #selector(formDidChange), for: .editingChanged)
        return code
    }()
    
    let goButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 10
        button.setTitle("Войти", for: .normal)
        button.addTarget(self, action: #selector(auth), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let getSmsButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 10
        button.setTitle("Получить смс", for: .normal)
        button.addTarget(self, action: #selector(getSmsButtonAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let  stackContainer: UIStackView = {
        let stackContainer = UIStackView()
        stackContainer.axis = .vertical
        stackContainer.distribution = .fill
        stackContainer.alignment = .fill
        stackContainer.spacing = 20
        return stackContainer
    }()
    var accessState = false
    
    var resendingAccess: LoginScreenViewOutputResendingAccess?{
        return execute(LoginScreenViewOutputResendingAccess.init(resendingAccess:),
                      with: accessState )
    }
    
    var outputData: LoginScreenViewOutputData?
    {
        return execute( LoginScreenViewOutputData.init(phone:code:),
                        with: phone.text,
                        code.text)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    @discardableResult
    func update(with inputData: LoginScreenViewInputData) -> Self {
        
        label.text = inputData.elementsState.labelText
        counter.text = inputData.textCounter
        counter.isHidden = inputData.resendingAccess
        getSmsButton.isHidden = inputData.elementsState.getSmsButton
        goButton.isHidden = inputData.elementsState.goButton
        code.isHidden = inputData.elementsState.codeTextField
        phone.isHidden = inputData.elementsState.phoneTextField
    
        return self
    }
    
    func setup(){
        backgroundColor = .green
        
        stackContainer.addArrangedSubview(label)
        stackContainer.addArrangedSubview(phone)
        stackContainer.addArrangedSubview(code)
        stackContainer.addArrangedSubview(counter)
        stackContainer.addArrangedSubview(getSmsButton)
        stackContainer.addArrangedSubview(goButton)
        
        let topSpacer = UILayoutGuide()
        
        addLayoutGuide(topSpacer)
        topSpacer.snp.makeConstraints {
            $0.leading.top.trailing.equalTo(safeAreaLayoutGuide)
            $0.height.equalToSuperview().multipliedBy(0.3)
        }
        
        addSubview(stackContainer)
        stackContainer.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(20)
            $0.top.equalTo(topSpacer.snp.bottom)
        }
    }
    
    
    @objc private func formDidChange() {
        
        outputData
            .map(LoginScreenViewEvent.formDidChange)
            .apply(self.events.send)
    }
    
    @objc private func getSmsButtonAction() {
        
        resendingAccess
            .map(LoginScreenViewEvent.codeButtonPressed)
            .apply(self.events.send)
        
    }
    
    @objc private func auth() {
        self.events.send(.authButtonPressed)
    }
    
}



struct LoginScreenViewInputData {
    let isCanSubmit: Bool
    let errorMessage: String?
    let crsfToken: String
    let crsfTokenState: Bool
    let resendingAccess: Bool
    let textCounter: String
    let elementsState: ViewElementsState
}

struct LoginScreenViewOutputData {
    var phone: String
    var code: String
    
}

struct LoginScreenViewOutputResendingAccess {
    let resendingAccess: Bool
}

enum LoginScreenViewEvent {
    case formDidChange(LoginScreenViewOutputData)
    case authButtonPressed
    case codeButtonPressed(LoginScreenViewOutputResendingAccess)
}
