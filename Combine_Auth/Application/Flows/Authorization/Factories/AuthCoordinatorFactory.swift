//
//  ItemListCoordinatorFactory.swift
//  Coordinator Example
//
//  Created by yuriy.p on 17.12.2020.
//

import Foundation

protocol AuthCoordinatorFactory {
    
    func makeAuthCoordinator(router: Router) -> AuthCoordinator
}

class AuthCoordinatorFactoryImpl: AuthCoordinatorFactory {
    
    private var authScreenFactory: AuthScreenFactory
    init(authScreenFactory: AuthScreenFactory) {
        self.authScreenFactory = authScreenFactory
    }
    func makeAuthCoordinator(router: Router) -> AuthCoordinator {
        return  AuthCoordinator(router: router, authScreenFactory: self.authScreenFactory)
         
    }
    
}
