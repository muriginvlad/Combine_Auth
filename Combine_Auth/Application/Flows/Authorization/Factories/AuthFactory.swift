
import Foundation

protocol AuthScreenFactory {    
    func makeAuthScreen() -> AuthViewController<AuthViewImpl>
    
//    func makeAuthScreen() -> TestVC
    
}

class AuthScreenFactoryImpl: AuthScreenFactory {
    
    private var loginProvider: LoginProvider
    
    init(loginProvider: LoginProvider) {
        self.loginProvider = loginProvider
    }
    
    func makeAuthScreen() -> AuthViewController<AuthViewImpl> {
        return  AuthViewController<AuthViewImpl>(loginProvider: self.loginProvider)
    }
    

//    func makeAuthScreen() -> TestVC {
//        return  TestVC(loginProvider: self.loginProvider)
//    }
    
}
