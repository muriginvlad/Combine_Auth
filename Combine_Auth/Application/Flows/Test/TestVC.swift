//
//  TestVC.swift
//  Combine_Auth
//
//  Created by Владислав on 26.02.2021.
//

import UIKit
import Combine

class TestVC: UIViewController {

    let events = PassthroughSubject<ViewEvent, Never>()
    
    var outputData: ViewOutputData?
    {
        return  ViewOutputData.init(string: "test")
    }

    
    typealias ErrorMessage = String
   
    private let loginProvider: LoginProvider
    
    var cancellables = Set<AnyCancellable>()

    let firstButton: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Войти", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: screen.size.height / 2 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(firstButtonPressing), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    let seconButton: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Войти", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: screen.size.height / 1.5 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(seconButtonPressing), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    
    
    init(loginProvider: LoginProvider) {
     self.loginProvider = loginProvider
        super.init(nibName: nil, bundle: nil)
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(firstButton)
        view.addSubview(seconButton)
        
        events.sink { state in
                        print(state)
                    } receiveValue: { response in
                        print(response)
                        print("Button pressed")
                    }
        .store(in: &cancellables)
        
    }
    

    @objc private func firstButtonPressing() {
        events.send(.firstButtonPressing)
       
    }
    
    @objc private func seconButtonPressing() {
        events.send(.seconButtonPressing)
    }
}

struct ViewOutputData {
    var string: String

}

enum ViewEvent {
    case firstButtonPressing
    case seconButtonPressing
}
