////
////  DetailsFlowFactory.swift
////  Coordinator Example
////
////  Created by yuriy.p on 16.12.2020.
////
//
//import Foundation
//
//protocol MainFlowModulesFactory {
//    
//    typealias MainModuleOutput = (MainViewController.Output) -> ()
//    
//    func makeMainModule(with value: String, and output: MainModuleOutput?) -> Presentable
//}
//
//class MainFlowFactoryImpl: MainFlowModulesFactory {
//    
//    func makeMainModule(with value: String, and output: MainModuleOutput?) -> Presentable {
//        
//        let vc = MainViewController()
//        vc.moduleOutputHandler = output
//        return vc
//    }
//
//}
