import Foundation

protocol LoginStatusProvider {
    var isLogin: Bool { get }
}

final class LoginStatusProviderImpl: LoginStatusProvider {
    
    private let authenticatorService: AuthenticationService
    
    init(authenticatorService: AuthenticationService) {
        self.authenticatorService = authenticatorService
    }
    
    var isLogin: Bool { authenticatorService.isLoggedIn }
    
}
