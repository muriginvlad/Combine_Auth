//
//  OAuthCredentialsEntity.swift
//  Refueler
//
//  Created by Boris Vecherkin on 29.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct AccessTokenEntity: Decodable {
    
    let value: String

}
