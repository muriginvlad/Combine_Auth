//
//  SignInCredentials.swift
//  Refueler
//
//  Created by Boris Vecherkin on 28.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct SignInRequest {
    
    var phone: String
    var code: String
    var csrfToken: String
    
    
    init?(phone: String, code: String, csrfToken: String) {
        //TODO: validator
        self.phone = phone
        self.code = code
        self.csrfToken = csrfToken
    }
}
