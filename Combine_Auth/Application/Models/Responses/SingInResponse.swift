//
//  SingInResponse.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 01.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct SignInResponse: Decodable {
    
    let accessToken: AccessTokenEntity
}


