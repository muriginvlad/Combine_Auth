//
//  DataResponse.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 11.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct DataResponse<T:Decodable>: Decodable {
    let data: T
}
