//
//  SendCodeResponse.swift
//  Refueler
//
//  Created by Владислав Мурыгин on 10.02.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation

struct SendCodeResponse: Decodable {
    
    let csrfToken: String
    let phone: String

}
