//
//  AppDelegate.swift
//  Combine_Auth
//
//  Created by Владислав on 18.02.2021.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var appCoordinator: Coordinator!
    var window: UIWindow?
    
    var authenticationService: AuthenticationService!
    var apiClient: ApiClient!
    var sessionRepository: SessionRepository!
    var session: Session!
    var decoder: JSONDecoder!
    var requestBuilder: RequestBuilder!
    var keychainWrapper: KeychainWrapper!
    var authCoordinatorFactory: AuthCoordinatorFactory!
    var authScreenFactory: AuthScreenFactory!
    var loginProvider: LoginProvider!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        self.requestBuilder = RequestBuilderImpl()
//        self.decoder = JSONDecoder()
//        self.session = Session.default
//        self.keychainWrapper = KeychainWrapperImpl.standard
//        self.apiClient = ApiClient(requestBuilder: self.requestBuilder, session: self.session, decoder: self.decoder)
//        self.sessionRepository = SessionRepositoryImpl(keychainWrapper: self.keychainWrapper)
//        self.authenticationService = AuthenticationServiceImpl(authenticationManager: self.apiClient, sessionRepository: self.sessionRepository)
//        self.loginProvider = LoginProviderImpl(authenticator: self.authenticationService)
//        self.authScreenFactory = AuthScreenFactoryImpl(loginProvider: self.loginProvider)
//        self.authCoordinatorFactory = AuthCoordinatorFactoryImpl(authScreenFactory: self.authScreenFactory)
//
//        let rootVC = UINavigationController()
//        rootVC.navigationBar.prefersLargeTitles = true
//        let router = RouterImp(rootController: rootVC)
//
//        appCoordinator = AppCoordinator(router: router, coordinatorFactory: self.authCoordinatorFactory)
//        appCoordinator.start()
//
//        window = UIWindow(frame: UIScreen.main.bounds)
//        window?.rootViewController = rootVC
//        window?.makeKeyAndVisible()
        
        
//        window = UIWindow(frame: UIScreen.main.bounds)
//        let homeViewController = UIViewController()
//        homeViewController.view.backgroundColor = UIColor.red
//        window!.rootViewController = homeViewController
//        window!.makeKeyAndVisible()
        
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

