//
//  SceneDelegate.swift
//  Combine_Auth
//
//  Created by Владислав on 18.02.2021.
//

import UIKit
import Alamofire
import SwiftKeychainWrapper

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var appCoordinator: Coordinator!
    
    var authenticationService: AuthenticationService!
    var apiClient: ApiClient!
    var sessionRepository: SessionRepository!
    var session: Session!
    var decoder: JSONDecoder!
    var requestBuilder: RequestBuilder!
    var keychainWrapper: KeychainWrapper!
    var authCoordinatorFactory: AuthCoordinatorFactory!
    var authScreenFactory: AuthScreenFactory!
    var loginProvider: LoginProvider!

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        self.requestBuilder = RequestBuilderImpl()
        self.decoder = JSONDecoder()
        self.session = Session.default
        self.keychainWrapper = KeychainWrapperImpl.standard
        self.apiClient = ApiClient(requestBuilder: self.requestBuilder, session: self.session, decoder: self.decoder)
        self.sessionRepository = SessionRepositoryImpl(keychainWrapper: self.keychainWrapper)
        self.authenticationService = AuthenticationServiceImpl(authenticationManager: self.apiClient, sessionRepository: self.sessionRepository)
        self.loginProvider = LoginProviderImpl(authenticator: self.authenticationService)
        self.authScreenFactory = AuthScreenFactoryImpl(loginProvider: self.loginProvider)
        self.authCoordinatorFactory = AuthCoordinatorFactoryImpl(authScreenFactory: self.authScreenFactory)


        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        let rootVC = UINavigationController()
        rootVC.navigationBar.prefersLargeTitles = true
        let router = RouterImp(rootController: rootVC)

        appCoordinator = AppCoordinator(router: router, coordinatorFactory: self.authCoordinatorFactory)
        appCoordinator.start()
        window?.rootViewController = rootVC
        
        window?.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

