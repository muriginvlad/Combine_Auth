//
//  SessionRepositoryImpl.swift
//  Refueler
//
//  Created by Boris Vecherkin on 27.01.2021.
//  Copyright © 2021 MOBILNAYA ZAPRAVKA, OOO. All rights reserved.
//

import Foundation
import Combine
import SwiftKeychainWrapper


class SessionRepositoryImpl: SessionRepository {
    
    private let keychainWrapper: KeychainWrapper
    
    init(keychainWrapper: KeychainWrapper) {
        self.keychainWrapper = keychainWrapper
    }
    
    var sessionId: String? {
        get {
            keychainWrapper.string(forKey: SessionRepositoryKeys.sessionId.rawValue, withAccessibility: nil)
        }
        set {
            if let newValue = newValue {
                keychainWrapper.set(newValue, forKey: SessionRepositoryKeys.sessionId.rawValue, withAccessibility: nil)
            } else {
                keychainWrapper.removeObject(forKey: SessionRepositoryKeys.sessionId.rawValue)
            }
        }
    }
}

private enum SessionRepositoryKeys: String {
    case sessionId
}
